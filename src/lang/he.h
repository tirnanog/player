/*
 * tngp/lang/he.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Hebrew dictionary for TirNanoG Player
 *
 */

"he",

"לא ניתן לאתחל את SDL",
"שגיאת הקצאת זיכרון",
"לא קובץ TNG חוקי",
"שדרג לשחקן האחרון כדי לשחק במשחק הזה",
"לא נמצאו משחקים, הורד כמה תחילה",
"אין ספריית מטמון, לעולם לא יקרה",
"לא ניתן להתחבר, נסה שוב לאחרונה",
"שגיאה בהורדת נכסים",

"הזן את מפתח רישיון המשחק",
"מתחבר לשרת",
"הורדת נכסים",

"מְקוֹמִי",
"באינטרנט",
"LAN",

"א"
