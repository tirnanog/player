TirNanoG Player
===============

Source of TirNanoG Player, the `tngp` executable, and TirNanoG Server, `tngs` (the latter is not part of the distributed
package, but you can create a separate package for it, see below). There's no separate repo for them, because they share
big part of the code base. Also contains stripped down version of the libraries these depend on. (As a bonus, the TirNanoG
Dumper `tngd` is also here, but it is not compiled by default).

I've made it so that all platform dependent code is either in tngp.c (main function) or in files.c (file operations). No other
source files should have `ifdef platform` guards or platform specific includes. I've almost succeded in this, except for
emscripten. Wasm is so limited and so poor in API compatibility, that I had to use `ifndef __EMSCRIPTEN__` all over the place.

Compilation
-----------

You have multiple options on how to compile the player. You can compile everything statically and produce a dependency-free,
single portable executable, "this is the way".

Or you can choose to compile dynamically, in which case the final executable will be much smaller, however it will
require certain .so files and DLLs to be installed on the end user's computer in order to run, so not recommended.

Makefile rules: `clean` will delete all objects for tnge, but won't touch the libraries. `distclean` will purge all
binaries, even in dependencies. `all` will compile, and finally `package` will create a distributable archive file.

### Linux

Requires only standard GNU toolchain (make, gcc and binutils or CLang).

#### Statically

As simple as:

```sh
$ make all package
```

NOTE: I had to remove wayland support from the static SDL2, because it did not compile due to a bug in wayland scanner and
xml2c converter (wait, what?), it wasn't SDL's fault. If wayland f*cked up tool gets fixed, remove `--disable-video-wayland`
from the Makefile.

Luckily you can compile everything statically in from the source tree, except SDL. To do this, use

```sh
$ USE_DYNSDL=1 make all package
```

This linking option also has a rule to create a debian package instead of a tarball:

```sh
$ USE_DYNSDL=1 make all deb
```

Creating a deb file uses only the standard GNU toolchain, no third party tools required.

#### Dynamically

Using your repo's package management system, install development versions of the dependencies. They are probably called:
`libsdl2-dev`, `libsdl2-mixer-dev`, `libvorbis-dev`, `libtheora-dev` (actual names vary on distros).

Then

```sh
$ USE_DYNLIBS=1 make all package
```

### Windows

Requires MSYS2 and MinGW.

#### Statically

```sh
$ make all package
```

Just like under Linux, you can compile everything statically in, except SDL. To do that, use

```sh
$ USE_DYNSDL=1 make all package
```

NOTE: the linker will complain about resources, something like "duplicate leaf: type: 10 (VERSION)", this is okay, it
happens because all libraries try to set their own version info. Don't care, `tngp.exe` will be generated just fine.

#### Dynamically

Use MSYS2's pacman to install the dependencies (listed above in Linux's Dynamically section), then

```sh
$ USE_DYNLIBS=1 make all package
```

#### Dynamically with official SDL2

Just for completeness, this should not be needed, as MSYS2's pacman should provide the correct version of SDL2.

Download the mingw version of the SDL2 library from the official repo, [libsdl.org](http://libsdl.org/download-2.0.php).
After unpack, it should contain a `i686-w64-mingw32` subdirectory. Specify the directory where you have extracted:

```sh
$ USE_DYNLIBS=1 USE_MINGWSDL=../../SDL2-mingw make all package
```

### MacOS

Known to have issues with compiling the libraries, **officially not supported**, but dynamically linking with the SDL2 framework
from its official repo, [libsdl.org](http://libsdl.org/download-2.0.php) should work (download the dmg under section development
and extract it to `/Library/Frameworks`). Everything else must be statically compiled in, if you have issues with this, let me
know.

```sh
$ USE_DYNSDL=1 make all package
```

### Emscripten

Go to the `../emscripten` directory, and run

```sh
$ make all package
```

There's no static / dynamic option, everything that's provided by emscripten is linked dynamically, everything that's not,
statically. Sadly emscripten does not provide theora, and you cannot compile the original source with emcc either. Luckily
I've found an unofficial port, the Makefile will download that and will compile it statically.

### Android

Go to the `../android-project` directory, and run

```sh
$ ./gradlew installRelease
```

There's no static / dynamic option, everything is generated into an .apk file. This version won't work with the statically
linked ogg.c amalgamation, so you'll need the original source in `../android-project/app/jni/ogg` and compile that as a dynamic
library.

TirNanoG Server
===============

The MMORPG server is **only supported on Linux**, although should compile out-of-the-box using MinGW too, but no guarantees.
To compile the TirNanoG Server, just run

```sh
$ make tngs
```

This will create the `tngs` executable (no static / dynamic linking options because it does not need SDL, nor vorbis, etc.).
This is a simple command line application which daemonizes itself (at least the Linux version, on Windows it can only run in
the foreground. Most Windows users do not know what a command line and CMD.EXE is, that's why it isn't supported).

If the built-in SSL certificate does not suit your needs for whatever reason, then you can simply delete the file and the
make rule will automatically regenerate it (you'll need to have `openssl` installed for this):

```sh
$ rm certs.h
$ make tngs
```

If the `tngs` executable was created successfully, then `make package` and `make deb` will automatically create tarball and
deb archives (respectively) for the TirNanoG Server as well. For example:

```sh
$ make all tngs package
$ USE_DYNSDL=1 make all tngs deb
```

Should create `tngp-*.tgz` as well as `tngs-*.tgz`, and `tngp_*.deb` just like `tngs_*.deb`.

TirNanoG Dumper
===============

This `tngd` tool is a little utility to dump sections and assets in a .tng file. Not compiled nor included in any package.
Compilation options (static, static but SDL dynamic, everything dynamic in this order):

```sh
$ make tngd
$ USE_DYNSDL=1 make tngd
$ USE_DYNLIBS=1 make tngd
```

That's all.
