@echo off

net session >nul 2>&1
if %errorLevel% == 0 (
    setlocal enableextensions
    assoc .tng=TirNanoG
    ftype TirNanoG="C:\Program Files\TirNanoG\tngp.exe" "%1"
) else (
    echo "You must run this script As Administrator."
    pause
)

rem we also create an lnk file for each game. Icon is embedded in tngp.exe, lnk files should reference that
