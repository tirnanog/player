/*
 * tngp/lang/hi.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Hindi dictionary for TirNanoG Player
 *
 */

"hi",

"SDL प्रारंभ करने में असमर्थ",
"मेमोरी आवंटन त्रुटि",
"मान्य TNG फ़ाइल नहीं है",
"इस गेम को खेलने के लिए नवीनतम प्लेयर में अपग्रेड करें",
"कोई गेम नहीं मिला, पहले कुछ डाउनलोड करें",
"कोई कैश निर्देशिका नहीं, कभी नहीं होनी चाहिए",
"कनेक्ट करने में असमर्थ, नवीनतम पुनः प्रयास करें",
"एसेट डाउनलोड करने में त्रुटि",

"गेम लाइसेंस कुंजी दर्ज करें",
"सर्वर से कनेक्ट हो रहा है",
"एसेट डाउनलोड करना",

"स्थानीय",
"ऑनलाइन",
"लैन",

"ऄ"
