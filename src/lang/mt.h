/*
 * tngp/lang/mt.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Maltese dictionary for TirNanoG Player
 *
 */

"mt",

"Ma jistax jinizjalizza SDL",
"Żball fl-allokazzjoni tal-memorja",
"Mhux fajl TNG validu",
"Aġġorna għall-aħħar plejer biex tilgħab din il-logħba",
"Ma nstab l-ebda logħob, niżżel l-ewwel",
"L-ebda direttorju tal-cache, qatt m'għandu jiġri",
"Ma tistax tikkonnettja, erġa' pprova aktar tard",
"Żball fit-tniżżil tal-assi",

"Daħħal iċ-ċavetta tal-liċenzja tal-logħba",
"Konnessjoni mas-server",
"Tniżżil ta' assi",

"Lokali",
"Online",
"LAN",

"A"
