/*
 * tngp/lang/bg.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Bulgarian dictionary for TirNanoG Player
 *
 */

"bg",

"Не може да се инициализира SDL",
"Грешка в разпределението на паметта",
"Не е валиден TNG файл",
"Надстройте до най-новия играч, за да играете тази игра",
"Няма намерени игри, първо изтеглете някои",
"Няма кеш директория, никога не трябва да се случва",
"Не може да се свърже, опитайте отново най-нов",
"Грешка при изтеглянето на активи",

"Въведете лицензен ключ за игра",
"Свързване със сървъра",
"Изтегляне на активи",

"Местен",
"На линия",
"LAN",

"Б"
