/*
 * tngp/tngp.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Main header for the TirNanoG Player
 *
 */

/* fix a bug in recent ndk-build */
#if !defined(__ANDROID__) && defined(__ANDROID_API__)
#define __ANDROID__ __ANDROID_API__
#endif
/* and another in emscripten */
#if !defined(__EMSCRIPTEN__) && defined(_emscripten_)
#define __EMSCRIPTEN__
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <SDL.h>
#define NETQ_CONFIG
#include "netq.h"
#include "zlib.h"
#include "tng.h"
#include "files.h"
#include "world.h"
#include "lang.h"
#include "data.h"
#include "ssfn.h"
#include "script.h"
#define STBI_NO_STDIO
#define STBI_NO_LINEAR
#define STBI_NO_HDR
#define STBI_ONLY_PNG
#include "stb_image.h"
#include "mbedtls/common.h"
#include "mbedtls/sha256.h"

enum {
    TAB_ERROR = -1,
    TAB_LAUNCHER = 0,
    TAB_DOWNLOAD,
    TAB_LICENSE,
    TAB_SPLASH,
    TAB_LOADING,
    TAB_FADING,
    TAB_CREDITS,
    TAB_INTRO,
    TAB_MAINMENU,
    TAB_CUTSCN,
    TAB_STARTGAME,
    TAB_GAME,

    TAB_EXIT
};

enum { HUD_NONE, HUD_MENU, HUD_CHAT, HUD_INV, HUD_MARKET, HUD_CRAFT };

extern SDL_Window *window;
extern SDL_Renderer *renderer;
extern SDL_Event event;
extern SDL_Texture *icon, *logo, *screen, *inpbg;
extern SDL_Rect lgopos, scroll;
extern uint32_t *scr_data, dialog_stop, main_timetotal, main_start, hud_belt[32];
extern uint16_t *main_scrshot;
extern volatile uint32_t main_tick;
extern volatile int main_draw;
extern int tab, win_w, win_h, scr_w, scr_h, scr_p, verbose, touch, audio, error_reason, cutscn_ret, main_cursor, main_takescrshot;
extern int ui_wt, ui_wb, ui_wl, ui_wr, ui_dt, ui_db, ui_dl, ui_dr, ui_ol, ui_or, main_musvol, main_sfxvol, main_spcvol, main_gptres;
extern int ui_lineh, ui_btnh, ui_sldb, ui_vbw, ui_vbs, ui_hbh, ui_hbs, ui_slots, ui_chatw, ui_chath, ui_input_gp, ui_input_cnt;
extern int game_a, cutscn_idx, cutscn_inloop, chooser_idx, craft_idx, dialog_idx, main_curx, main_cury, media_verbose;
extern int map_half, map_mm, map_halfw, map_halfh, map_halftw, map_halfth, map_w, map_h, client_eqchange;
extern char detlng[3], *main_fn, *client_url, *ui_input_str, *ui_input_cur;
extern const char *main_me, *main_url;
extern world_sprite_t *main_cursors[5];
extern world_scene_t game_scene, cutscn_scene;

/* jsonc.c */
char *json_get(const char *jsonstr, char *key);

/* tngp.c */
int  main_textwidth(char *str);
void main_text(SDL_Texture *dst, int x, int y, char *str);
void main_splash();
void main_switchtab(int newtab);
void main_error(int msg);
void *main_alloc(size_t size);
void *main_realloc(void *ptr, size_t size);
void main_open_url(const char *url);
void main_win(int w, int h);
void main_savegame();
void main_loadconfig();
void main_saveconfig();
void main_gettick();
int  main_init(char *fn, char *prog);

/* ui.c */
void ui_init();
int  ui_anim(int type, int nframe, int currframe);
void ui_fit(int mw, int mh, int sw, int sh, int *w, int *h);
char *ui_fitstr(char *str, int w, int fnt, int *nl, char *user, int *attrs);
void ui_set(uint32_t *d, uint32_t *s, uint32_t *p);
void ui_blit(uint32_t *dst, int dw, int dh, int dp, int x, int y, uint32_t *src, int sx, int sy, int sw, int sh, int sp, int g);
SDL_Texture *ui_window(int w, int h, int *tabs, int active, char *title, SDL_Rect *border, int *tabx, int am, int aa);
SDL_Texture *ui_dlgwin(int w, int h, char *str, SDL_Rect *border);
void ui_keybox(uint32_t *dst, int p, int x, int y, int w, int h, char *key, int sel, int pressed);
void ui_small(uint32_t *dst, int dw, int dh, int dp, int x, int y, uint32_t c, char *str);
void ui_text(uint32_t *dst, int dw, int dh, int dp, int x, int y, ssfn_t *font, uint32_t color, char *str);
void ui_line(uint32_t *dst, int dw, int dh, int dp, int x0, int y0, int x1, int y1, uint32_t color);
void ui_tri(uint32_t *dst, int dw, int dh, int dp, int x0, int y0, int x1, int y1, int x2, int y2, uint32_t color);
void ui_box(uint32_t *dst, int dw, int dh, int dp, int x, int y, int w, int h, uint32_t l, uint32_t b, uint32_t d);
void ui_button(uint32_t *dst, int dw, int dh, int dp, int x, int y, int w, int type, char *str);
void ui_chkbox(uint32_t *dst, int dw, int dh, int dp, int x, int y, int checked);
void ui_number(uint32_t *dst, int dw, int dh, int dp, int x, int y, int w, int type, int val);
void ui_option(uint32_t *dst, int dw, int dh, int dp, int x, int y, int w, int type, char **opts, int val);
void ui_slider(uint32_t *dst, int dw, int dh, int dp, int x, int y, int w, int val, int max);
void ui_textinp(uint32_t *dst, int dw, int dh, int dp, int x, int y, int w, int type, char *str);
void ui_vscr(uint32_t *dst, int dw, int dh, int dp, int x, int y, int h, int val, int max);
void ui_hscr(uint32_t *dst, int dw, int dh, int dp, int x, int y, int w, int val, int max);

/* ui_input.c */
void ui_input_init();
void ui_input_start(int x, int y, int w, int h, char *label, char *str, int maxlen, int pass);
void ui_input_addkey(char *key);
int  ui_input_ctrl();
void ui_input_view();

/* spr.c */
void spr_init();
void spr_free();
void spr_fade(int a);
SDL_Texture *spr_getbg(int idx, int *w, int *h);
world_sprite_t *spr_get(int cat, int idx, int dir);
void spr_render(int x, int y, world_sprite_t *spr, int frame, int flip, int w, int h, int a);
void spr_blit(uint32_t *dst, int dw, int dh, int dp, int x, int y, int w, int h, world_sprite_t *spr, int frame, uint32_t *pal);

/* media.c */
void media_init();
void media_free();
void media_open();
void media_close();
void media_freespeech();
void media_mic_start();
void media_mic_stop();
void media_chime(int vol);
void media_playerr();
void media_playsel();
void media_playclk();
void media_playmus(world_asset_t *once, world_asset_t *loop);
void media_stopmus(int dur);
void media_fadeout(int dur);
void media_playinv(int idx);
void media_playspc(int idx, int tx, int ty);
void media_playsnd(int idx, int tx, int ty);
void media_playaud(int hz, int fmt, uint8_t *buf, int len);

/* map.c */
void map_init();
void map_tile2map(int srcx, int srcy, int *dstx, int *dsty);
void map_map2tile(int srcx, int srcy, int *dstx, int *dsty);
void map_dist(world_map_t **maps, world_entity_t *this, world_entity_t *ent, int *x, int *y);
void map_load(world_map_t **maps, int neightbor, int map);
void map_unload(world_map_t **maps);
void map_scroll(world_map_t **maps, int dx, int dy);

/* font.c */
void font_init();
void font_free();
ssfn_t *font_get(int idx);
ssfn_t *font_custom(world_font_t *fnt);

/* launcher.c */
void launcher_init();
void launcher_free();
int  launcher_ctrl();
void launcher_view();

/* license.c */
void license_init();
void license_free();
int  license_ctrl();
void license_view();

/* client.c */
void client_init();
void client_free();
int  client_ctrl();
void client_view();
void client_close();
int  client_register(char *user, char *pass, int32_t *opts, int32_t *attrs);
int  client_login(char *user, char *pass);
int  client_send(int type, uint8_t *buf, int len);
void client_recv();
void client_sendupdate();

/* alert.c */
void alert_init(int idx);
void alert_free();
void alert_view();

/* cutscn.c */
int  cutscn_placenpc(int idx, int x, int y, int d);
void cutscn_init(int idx, int *flag);
void cutscn_start();
void cutscn_free();
void cutscn_run();
int  cutscn_loadmap(int map, int x, int y, int daytime);
void cutscn_setmap();
void cutscn_freemap();
int  cutscn_ctrl();
void cutscn_view();

/* chooser.c */
void chooser_init(int idx, int *flag, int *regs);
void chooser_free();
int  chooser_ctrl();
void chooser_view(SDL_Rect *dst);

/* craft.c */

/* dialog.c */
void dialog_init(int idx, int *flag, int *ret);
void dialog_free();
int  dialog_ctrl();
void dialog_view();

/* hud.c */
void hud_init();
void hud_free();
void hud_showtitle(char *str);
void hud_market(int npc);
void hud_craft(int craft);
int  hud_ctrl();
void hud_view();

/* mainmenu.c */
void mainmenu_init();
void mainmenu_free();
int  mainmenu_ctrl();
void mainmenu_view();

/* credits.c */
void credits_init();
void credits_free();
int  credits_ctrl();
void credits_view();

/* game.c */
void game_init();
void game_exit();
void game_free();
void game_fadeout();
void game_fadein();
void game_run();
int  game_ctrl();
void game_view();
void game_useitem(int idx, int belt);
void game_loadmap(int map_id, int x, int y);
void game_setmap();
void game_freemap();
void game_map(world_scene_t *scene, int d, int a);
