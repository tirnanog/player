/*
 * tngp/lang/ca.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Catalan dictionary for TirNanoG Player
 *
 */

"ca",

"No es pot inicialitzar SDL",
"Error d'assignació de memòria",
"No és un fitxer TNG vàlid",
"Actualitza a l'últim jugador per jugar a aquest joc",
"No s'han trobat jocs, primer descarregueu-ne alguns",
"No hi ha cap directori de memòria cau, no hauria de passar mai",
"No es pot connectar, torna-ho a provar més tard",
"S'ha produït un error en baixar els recursos",

"Introduïu la clau de llicència del joc",
"Connectant al servidor",
"Descàrrega d'actius",

"Local",
"En línia",
"LAN",

"A"
