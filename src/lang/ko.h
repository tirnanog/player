/*
 * tngp/lang/ko.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Korean dictionary for TirNanoG Player
 *
 */

"ko",

"SDL을 초기화할 수 없습니다.",
"메모리 할당 오류",
"유효한 TNG 파일이 아닙니다.",
"이 게임을 플레이하려면 최신 플레이어로 업그레이드하세요.",
"게임을 찾을 수 없습니다. 먼저 게임을 다운로드하세요.",
"캐시 디렉토리 없음, 절대 발생해서는 안 됨",
"연결할 수 없습니다. 최신으로 다시 시도하세요.",
"자산 다운로드 오류",

"게임 라이센스 키 입력",
"서버에 연결",
"자산 다운로드",

"현지",
"온라인",
"LAN",

"ᄀ"
