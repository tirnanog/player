/*
 * tngp/lang/tr.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Turkish dictionary for TirNanoG Player
 *
 */

"tr",

"SDL başlatılamıyor",
"Bellek ayırma hatası",
"Geçerli bir TNG dosyası değil",
"Bu oyunu oynamak için en son oyuncuya yükseltin",
"Oyun bulunamadı, önce bazılarını indirin",
"Önbellek dizini yok, asla olmamalı",
"Bağlantı kurulamıyor, en son tekrar deneyin",
"Varlıklar indirilirken hata oluştu",

"Oyun lisans anahtarını girin",
"Sunucuya baglanıyor",
"Varlıkları indirme",

"Yerel",
"Çevrimiçi",
"LAN",

"A"
