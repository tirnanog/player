/*
 * tngp/lang/es.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Spanish dictionary for TirNanoG Player
 *
 */

"es",

"No se puede inicializar SDL",
"Error de asignación de memoria",
"No es un archivo TNG válido",
"Actualizar al último jugador para jugar este juego",
"No se encontraron juegos, descargue algunos primero",
"Sin directorio de caché, nunca debería suceder",
"No se puede conectar, vuelva a intentarlo más reciente",
"Error al descargar activos",

"Introduce la clave de licencia del juego",
"Conectando al servidor",
"Descarga de activos",

"Local",
"En línea",
"LAN",

"A"
