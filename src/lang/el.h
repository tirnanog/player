/*
 * tngp/lang/el.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Greek dictionary for TirNanoG Player
 *
 */

"el",

"Δεν είναι δυνατή η προετοιμασία του SDL",
"Σφάλμα εκχώρησης μνήμης",
"Δεν είναι έγκυρο αρχείο TNG",
"Κάντε αναβάθμιση στον πιο πρόσφατο παίκτη για να παίξετε αυτό το παιχνίδι",
"Δεν βρέθηκαν παιχνίδια, κατεβάστε πρώτα μερικά",
"Δεν υπάρχει κατάλογος κρυφής μνήμης, δεν πρέπει ποτέ να συμβεί",
"Δεν είναι δυνατή η σύνδεση, δοκιμάστε ξανά το τελευταίο",
"Σφάλμα κατά τη λήψη στοιχείων",

"Εισαγάγετε το κλειδί άδειας παιχνιδιού",
"Σύνδεση με διακομιστή",
"Λήψη στοιχείων",

"Τοπικός",
"Σε σύνδεση",
"LAN",

"Γ"
