/*
 * tngp/lang/ro.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Romanian dictionary for TirNanoG Player
 *
 */

"ro",

"Nu se poate inițializa SDL",
"Eroare de alocare a memoriei",
"Nu este un fișier TNG valid",
"Faceți upgrade la cel mai recent jucător pentru a juca acest joc",
"Nu s-au găsit jocuri, mai întâi descărcați unele",
"Niciun director cache, nu ar trebui să se întâmple niciodată",
"Nu se poate conecta, reîncercați cel mai recent",
"Eroare la descărcarea materialelor",

"Introduceți cheia de licență a jocului",
"Conectare la server",
"Descărcarea activelor",

"Local",
"Pe net",
"LAN",

"A"
