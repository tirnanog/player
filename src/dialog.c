/*
 * tngp/dialog.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Conversation dialog
 *
 */

#include "tngp.h"

int dialog_idx = -1, *dialog_flag = NULL, *dialog_ret = NULL;
uint32_t dialog_stop = 0;

/**
 * Initialize dialog
 */
void dialog_init(int idx, int *flag, int *ret)
{
    if(idx >= 0 && idx < world.numdlg) {
        dialog_idx = idx;
        if(flag) { dialog_flag = flag; *flag = 1; }
        if(ret) dialog_ret = ret;
    }
}

/**
 * Free resources
 */
void dialog_free()
{
    dialog_idx = -1;
    if(dialog_flag) { *dialog_flag = 0; dialog_flag = NULL; }
    dialog_ret = NULL;
}

/**
 * Controller
 */
int dialog_ctrl()
{
    switch(event.type) {
        case SDL_KEYDOWN:
            ui_input_gp &= ~1;
            switch(event.key.keysym.sym) {
                case SDLK_AC_BACK:
                case SDLK_F10:
                case SDLK_BACKSPACE:
                case SDLK_ESCAPE: break;

            }
        break;
    }
    return 1;
}

/**
 * View layer
 */
void dialog_view()
{
    if(!main_draw) return;
}
