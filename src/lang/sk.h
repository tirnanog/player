/*
 * tngp/lang/sk.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Slovak dictionary for TirNanoG Player
 *
 */

"sk",

"Nedá sa inicializovať SDL",
"Chyba pri prideľovaní pamäte",
"Neplatný súbor TNG",
"Ak chcete hrať túto hru, inovujte na najnovšieho hráča",
"Nenašli sa žiadne hry, najprv si stiahnite nejaké",
"Žiadny adresár vyrovnávacej pamäte, nikdy by sa to nemalo stať",
"Nedá sa pripojiť, skúste to znova",
"Chyba pri sťahovaní aktív",

"Zadajte licenčný kľúč hry",
"Pripája sa k serveru",
"Sťahovanie aktív",

"Miestne",
"Online",
"LAN",

"A"
