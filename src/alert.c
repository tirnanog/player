/*
 * tngp/alert.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Alert popup
 *
 */

#include "tngp.h"

static SDL_Texture *alert_txt = NULL;
static SDL_Rect alert_dst;
static uint32_t alert_fade;

/**
 * Initialize alert message
 */
void alert_init(int idx)
{
    ssfn_t *font;
    uint32_t *dst;
    int p, l, t;
    char *str;

    if(alert_txt) { SDL_DestroyTexture(alert_txt); alert_txt = NULL; }
    if(idx >= 0 && idx < world.numalr && world.text && world.alerts[idx].trname >= 0 && world.alerts[idx].trname < world.numtext &&
      world.players && world.numplayers > 0) {
        str = world_parsestr(world.text[world.alerts[idx].trname], world.players[0].name, world.players[0].data.attrs);
        if(!str || !*str) return;
        memset(&alert_dst, 0, sizeof(alert_dst));
        font = font_get(FNT_ALERT);
        if(ssfn_bbox(font, str, &alert_dst.w, &alert_dst.h, &l, &t) == SSFN_OK) {
            alert_dst.w += l + 2; alert_dst.h += 2; font->style &= ~SSFN_STYLE_A;
            alert_txt = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, alert_dst.w, alert_dst.h);
            if(alert_txt) {
                SDL_SetTextureBlendMode(alert_txt, SDL_BLENDMODE_BLEND);
                SDL_LockTexture(alert_txt, NULL, (void**)&dst, &p);
                memset(dst, 0, p * alert_dst.h);
                ui_text(dst, alert_dst.w, alert_dst.h, p, 2, t + 2, font, 0x7F000000, str);
                ui_text(dst, alert_dst.w, alert_dst.h, p, 0, t, font, world.alerts[idx].color, str);
                SDL_UnlockTexture(alert_txt);
                alert_fade = main_tick;
                if(world.alerts[idx].snd >= 0 && world.alerts[idx].snd < world.numsound)
                    media_playsnd(world.alerts[idx].snd, 0, 0);
            }
        }
        free(str);
    }
}

/**
 * Free resources
 */
void alert_free()
{
    if(alert_txt) { SDL_DestroyTexture(alert_txt); alert_txt = NULL; }
    memset(&alert_dst, 0, sizeof(alert_dst));
}

/**
 * View layer
 */
void alert_view()
{
    SDL_Rect dst;
    int a = 255, i;

    if(!main_draw) return;
    if(alert_txt) {
        i = main_tick - alert_fade;
        if(i > world.alert_dt * 10) {
            SDL_DestroyTexture(alert_txt); alert_txt = NULL;
        } else {
            dst.w = alert_dst.w; dst.h = alert_dst.h;
            if(i < world.alert_fi * 10 && world.alert_fi > 0) {
                a = i * 255 / (world.alert_fi * 10);
                dst.w = alert_dst.w * i / (world.alert_fi * 10);
                dst.h = alert_dst.h * i / (world.alert_fi * 10);
            } else
            if(i >= world.alert_fi * 10 && i < world.alert_fo * 10) a = 255; else
            if(i >= world.alert_fo && world.alert_fo < world.alert_dt)
                a = 255 - ((i - world.alert_fo * 10) * 255 / ((world.alert_dt - world.alert_fo) * 10));
            if(a > game_a) a = game_a;
            dst.x = (win_w - dst.w) / 2;
            dst.y = (win_h - dst.h) / 2;
            SDL_SetTextureAlphaMod(alert_txt, a);
            SDL_RenderCopy(renderer, alert_txt, NULL, &dst);
        }
    }
}
