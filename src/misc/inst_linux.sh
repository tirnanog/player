#!/bin/sh

if [ "`whoami`" != "root" ]; then
    echo "You must run this script as root"
    exit
fi

cp tngp16.png /usr/share/icons/hicolor/16x16/mimetypes/application-x-tirnanog.png 2>/dev/null || true
cp tngp24.png /usr/share/icons/hicolor/24x24/mimetypes/application-x-tirnanog.png 2>/dev/null || true
cp tngp.png /usr/share/icons/hicolor/32x32/mimetypes/application-x-tirnanog.png 2>/dev/null || true
cp tngp48.png /usr/share/icons/hicolor/48x48/mimetypes/application-x-tirnanog.png 2>/dev/null || true
cp tngp64.png /usr/share/icons/hicolor/64x64/mimetypes/application-x-tirnanog.png 2>/dev/null || true
cp tngp128.png /usr/share/icons/hicolor/128x128/mimetypes/application-x-tirnanog.png 2>/dev/null || true

if [ -f /etc/mime.types ]; then
    grep application/x-tirnanog /etc/mime.types >/dev/null 2>&1
    if [ $? == 1 ]; then
        printf "application/x-tirnanog\t\t\t\ttng" >>/etc/mime.types
    fi
fi
if [ -f /etc/mailcap ]; then
    grep application/x-tirnanog /etc/mailcap >/dev/null 2>&1
    if [ $? == 1 ]; then
        printf "application/x-tirnanog; /usr/bin/tngp %%s" >>/etc/mailcap
    fi
fi

cat <<EOF >/usr/share/mime/packages/application-x-tirnanog.xml
<?xml version="1.0" encoding="UTF-8"?>
<mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
   <mime-type type="application/x-tirnanog">
      <comment>TirNanoG</comment>
      <glob pattern="*.tng"/>
      <icon name="tngp"/>
   </mime-type>
</mime-info>
EOF
update-mime-database /usr/share/mime

# we also create a desktop file for each game
