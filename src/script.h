/*
 * tngp/script.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Scripting support
 *
 */

#define SCRIPT_STACKSIZE    128
#define SCRIPT_CALLSIZE     32
#define SCRIPT_ERR          -2147483647     /* script made a boo-boo */
#define SCRIPT_EXIT         -2147483646     /* script called exit() */
#define SCRIPT_AGAIN        -2147483645     /* script exited temporarily, needs to be executed again */

enum { SCRIPT_CUTSCN, SCRIPT_STARTUP, SCRIPT_ATTR, SCRIPT_DEFACT, SCRIPT_NPC, SCRIPT_OBJ, SCRIPT_QUEST, SCRIPT_OBJCALL };
#define SCRIPT_CALLER(a,b) (((b) << 8) | ((a) & 7))
#define SCRIPT_CALLERFUNC(a,b,c) (((b) << 8) | (((c) & 31) << 3) | ((a) & 7))

int  script_load(int caller, uint8_t *bc, int bclen, int tng, world_entity_t *this, world_entity_t *oppo);
int  script_cron(int caller, uint8_t *bc, int bclen, int tng, world_entity_t *this, world_entity_t *oppo, int interval);
void script_setattr(world_entity_t *data, int attr, int value);
int  script_getattr(world_entity_t *data, int attr);
int  script_top();
void script_init();
void script_free();
void script_crons();
void script_animactors();

/* called by quest() script command, different for tngp and tngs */
extern void main_addquest(world_entity_t *ent, int idx, int complete);
