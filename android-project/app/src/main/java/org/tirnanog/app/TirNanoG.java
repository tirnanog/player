package org.tirnanog.app;
import org.libsdl.app.SDLActivity;

public class TirNanoG extends SDLActivity {

    protected String[] getLibraries() {
        return new String[] {
            "hidapi",
            "SDL2",
            "SDL2_mixer",
            "theora",
            "vorbis",
            "ogg",
            "main"
        };
    }

}
