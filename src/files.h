/*
 * tngp/files.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Platform dependent file function definitions
 *
 */

#include <sys/stat.h>
#include <dirent.h>
#include <limits.h>
#include "mbedtls/common.h"
#include "mbedtls/platform.h"
#include "mbedtls/platform_util.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/x509.h"
#include "mbedtls/ssl.h"
#include "mbedtls/net_sockets.h"
#include "mbedtls/error.h"

#ifdef __WIN32__
#define SEP "\\"
#define GAMESDIR "C:\\Program Files"
#else
#define SEP "/"
#ifdef __ANDROID__
#define GAMESDIR files_games
#else
#define GAMESDIR "/usr/share/games"
#endif
#endif

#ifndef PATH_MAX
# ifdef MAX_PATH
#  define PATH_MAX MAX_PATH
# else
#  define PATH_MAX 4096
# endif
#endif
#ifndef FILENAME_MAX
# define FILENAME_MAX 256
#endif

typedef struct {
    mbedtls_ssl_context ssl;
    mbedtls_ssl_config conf;
    mbedtls_net_context server_fd;
    mbedtls_ctr_drbg_context ctr_drbg;
    FILE *f;
    size_t filesize;
    size_t pos;
    size_t ilen;
    size_t olen;
    uint8_t *buf;
} files_t;

typedef struct {
    char *gameid;
    char *name;
    char *url;
    int w, h, s;
    uint8_t *png, *img;
} files_game_t;

#ifndef _MSC_VER
#define _pack __attribute__((packed))
#else
#define _pack
#pragma pack(push)
#pragma pack(1)
#endif

#define ZIP_DATA_STORE      0
#define ZIP_DATA_DEFLATE    8

#define ZIP_LOCAL_MAGIC     0x04034b50
typedef struct {
    uint32_t    magic;
    uint16_t    version;
    uint16_t    flags;
    uint16_t    method;
    uint32_t    mtime;
    uint32_t    crc32;
    uint32_t    comp;
    uint32_t    orig;
    uint16_t    fnlen;
    uint16_t    extlen;
} _pack zip_local_t;

#define ZIP_EXTZIP64_MAGIC  0x0001
typedef struct {
    uint16_t    magic;
    uint16_t    size;
    uint64_t    orig;
    uint64_t    comp;
    uint64_t    offs;
    uint32_t    disk;
} _pack zip_ext64_t;

#ifdef _MSC_VER
#pragma pack(pop)
#else
#undef _pack
#endif

/* common paths */
extern char *files_games, *files_cache, *files_config, *files_saves;

void files_init();
void files_free();
int files_exists(char *path);
uint64_t files_size(char *path);
void files_mkdir(char *path);
FILE *files_open(const char *fn, const char *mode);
void files_seek(FILE *f, int64_t offs);
int files_read(FILE *f, void *buf, int size);
char **files_getdir(char *path, char *ext, int *num);
void files_freedir(char ***list, int *num);
char *files_readfile(char *fn, int *size);
files_t *files_ssl_open(char *url, char *method, char *param, char *add, char **cookie);
int files_ssl_read(files_t *f);
int files_ssl_close(files_t *f);
files_game_t *files_getgames(int *num);
void files_freegames(files_game_t **games, int *num);
