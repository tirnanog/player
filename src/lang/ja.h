/*
 * tngp/lang/ja.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Japanese dictionary for TirNanoG Player
 *
 */

"ja",

"SDLを初期化できません",
"メモリ割り当てエラー",
"有効なTNGファイルではありません",
"このゲームをプレイするには、最新のプレーヤーにアップグレードしてください",
"ゲームが見つかりません。最初にダウンロードしてください",
"キャッシュディレクトリはありません、決して起こらないはずです",
"接続できません。最新の再試行を行ってください",
"アセットのダウンロード中にエラーが発生しました",

"ゲームライセンスキーを入力してください",
"サーバーにつなげる",
"アセットのダウンロード",

"地元",
"オンライン",
"LAN",

"あ"
