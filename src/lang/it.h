/*
 * tngp/lang/it.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Italian dictionary for TirNanoG Player
 *
 */

"it",

"Impossibile inizializzare SDL",
"Errore di allocazione della memoria",
"File TNG non valido",
"Aggiorna all'ultimo giocatore per giocare a questo gioco",
"Nessun gioco trovato, scaricane prima alcuni",
"Nessuna directory cache, non dovrebbe mai accadere",
"Impossibile connettersi, riprova più tardi",
"Errore durante il download delle risorse",

"Inserisci la chiave di licenza del gioco",
"Connessione al server",
"Download delle risorse",

"Locale",
"In linea",
"LAN",

"A"
