/*
 * tngp/lang/pt.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Portuguese dictionary for TirNanoG Player
 *
 */

"pt",

"Não foi possível inicializar o SDL",
"Erro de alocação de memória",
"Não é um arquivo TNG válido",
"Atualize para o jogador mais recente para jogar este jogo",
"Nenhum jogo encontrado, baixe alguns primeiro",
"Nenhum diretório de cache, nunca deve acontecer",
"Não foi possível conectar, tente novamente",
"Erro ao fazer download de ativos",

"Digite a chave de licença do jogo",
"Conectando ao servidor",
"Baixando recursos",

"Local",
"Online",
"LAN",

"A"
