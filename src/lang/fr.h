/*
 * tngp/lang/fr.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief French dictionary for TirNanoG Player
 *
 */

"fr",

"Impossible d'initialiser SDL",
"Erreur d'allocation de mémoire",
"Fichier TNG non valide",
"Passez au dernier joueur pour jouer à ce jeu",
"Aucun jeu trouvé, téléchargez-en d'abord",
"Pas de répertoire de cache, ne devrait jamais arriver",
"Impossible de se connecter, réessayez au plus tard",
"Erreur lors du téléchargement des éléments",

"Entrez la clé de licence du jeu",
"Connexion au serveur",
"Téléchargement de ressources",

"Local",
"En ligne",
"LAN",

"A"
