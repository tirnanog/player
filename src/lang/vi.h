/*
 * tngp/lang/vi.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Vietnamese dictionary for TirNanoG Player
 *
 */

"vi",

"Không thể khởi tạo SDL",
"Cấp phát bộ nhớ lỗi",
"Không phải là tệp TNG hợp lệ",
"Nâng cấp lên trình phát mới nhất để chơi trò chơi này",
"Không tìm thấy trò chơi nào, hãy tải xuống một số trò chơi trước tiên",
"Không có thư mục bộ nhớ cache, sẽ không bao giờ xảy ra",
"Không thể kết nối, hãy thử lại mới nhất",
"Lỗi khi tải nội dung xuống",

"Nhập khóa cấp phép trò chơi",
"Kết nối tới máy chủ",
"Tải xuống nội dung",

"Địa phương",
"Trực tuyến",
"LAN",

"A"
