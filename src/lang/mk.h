/*
 * tngp/lang/mk.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Macedonian dictionary for TirNanoG Player
 *
 */

"mk",

"Не може да се иницијализира SDL",
"Грешка во распределбата на меморијата",
"Не е валидна TNG-датотека",
"Надградете го на најновиот играч што ќе ја игра оваа игра",
"Не се пронајдени игри, прво преземете некои",
"Нема директориум за кеш, никогаш не треба да се случи",
"Не може да се поврзе, обидете се повторно најдоцна",
"Грешка при преземањето средства",

"Внесете го клучот за лиценца на играта",
"Се поврзува со серверот",
"Преземање средства",

"Локално",
"Онлајн",
"LAN",

"Б"
