/*
 * tngp/lang/ga.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Irish dictionary for TirNanoG Player
 *
 */

"ga",

"Ní féidir SDL a thúsú",
"Earráid leithdháilte cuimhne",
"Ní comhad TNG bailí é",
"Uasghrádaigh go dtí an t-imreoir is déanaí chun an cluiche seo a imirt",
"Ní bhfuarthas aon cluichí, íoslódáil roinnt ar dtús",
"Níor cheart go dtarlódh aon eolaire taisce",
"Ní féidir ceangal a dhéanamh, bain triail eile as is déanaí",
"Earráid agus sócmhainní á n-íoslódáil",

"Íosluchtaigh eochair ceadúnas le haghaidh an cluiche",
"Ag ceangal leis an bhfreastalaí",
"Sócmhainní a íoslódáil",

"Áitiúil",
"Ar líne",
"LAN",

"A"
