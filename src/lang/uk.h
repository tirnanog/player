/*
 * tngp/lang/uk.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Ukrainian dictionary for TirNanoG Player
 *
 */

"uk",

"Не вдається ініціалізувати SDL",
"Помилка розподілу пам'яті",
"Недійсний файл TNG",
"Щоб грати в цю гру, перейдіть до останньої версії",
"Ігор не знайдено, спершу завантажте деякі",
"Немає каталогу кешу, ніколи не повинно бути",
"Не вдається підключитися, повторіть спробу останньою",
"Помилка завантаження об’єктів",

"Введіть ліцензійний ключ гри",
"Підключення до сервера",
"Завантаження активів",

"Місцевий",
"Онлайн",
"ЛАН",

"Б"
