/*
 * tngp/lang/cz.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Czech dictionary for TirNanoG Player
 *
 */

"cz",

"Nelze inicializovat SDL",
"Chyba alokace paměti",
"Neplatný soubor TNG",
"Chcete-li hrát tuto hru, upgradujte na nejnovějšího hráče",
"Nebyly nalezeny žádné hry, nejprve si nějaké stáhněte",
"Žádný adresář mezipaměti, nikdy by se to nemělo stát",
"Nelze se připojit, zkuste to znovu",
"Chyba při stahování podkladů",

"Zadejte licenční klíč hry",
"Připojení k serveru",
"Stahování aktiv",

"Místní",
"Online",
"LAN",

"A"
