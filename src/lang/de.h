/*
 * tngp/lang/de.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief German dictionary for TirNanoG Player
 *
 */

"de",

"SDL kann nicht initialisiert werden",
"Speicherzuweisungsfehler",
"Keine gültige TNG-Datei",
"Aktualisieren Sie auf den neuesten Player, um dieses Spiel zu spielen",
"Keine Spiele gefunden, laden Sie zuerst einige herunter",
"Kein Cache-Verzeichnis, sollte nie passieren",
"Verbindung nicht möglich, versuchen Sie es später erneut",
"Fehler beim Herunterladen von Assets",

"Geben Sie den Lizenzschlüssel für das Spiel ein",
"Verbindung zum Server herstellen",
"Assets herunterladen",

"Lokal",
"Online",
"LAN",

"A"
