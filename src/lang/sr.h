/*
 * tngp/lang/sr.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Serbian dictionary for TirNanoG Player
 *
 */

"sr",

"Није могуће иницијализовати СДЛ",
"Грешка у додели меморије",
"Није важећа ТНГ датотека",
"Надоградите на најновији играч да бисте играли ову игру",
"Нису пронађене игре, прво преузмите неке",
"Нема кеш директоријума, никада не би требало да се деси",
"Повезивање није успело, покушајте поново",
"Грешка при преузимању материјала",

"Унесите лиценцни кључ игре",
"Повезивање на сервер",
"Преузимање средстава",

"Локални",
"Онлине",
"ЛАН",

"Б"
