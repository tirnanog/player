/*
 * tngp/lang/bn.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Bengali dictionary for TirNanoG Player
 *
 */

"bn",

"SDL আরম্ভ করতে অক্ষম",
"মেমরি বরাদ্দ ত্রুটি",
"একটি বৈধ TNG ফাইল নয়",
"এই গেমটি খেলতে সর্বশেষ প্লেয়ারে আপগ্রেড করুন",
"কোনো গেম পাওয়া যায়নি, প্রথমে কিছু ডাউনলোড করুন",
"কোন ক্যাশে ডিরেক্টরি, কখনই ঘটতে হবে না",
"সংযোগ করতে অক্ষম, সর্বশেষ আবার চেষ্টা করুন",
"সম্পদ ডাউনলোড করার সময় ত্রুটি",

"গেম লাইসেন্স কী লিখুন",
"সার্ভারে সংযোগ করা হচ্ছে",
"সম্পদ ডাউনলোড করা হচ্ছে",

"স্থানীয়",
"অনলাইন",
"ল্যান",

"অ"
