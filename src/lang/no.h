/*
 * tngp/lang/no.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Norwegian dictionary for TirNanoG Player
 *
 */

"no",

"Kan ikke initialisere SDL",
"Minnetildelingsfeil",
"Ikke en gyldig TNG-fil",
"Oppgrader til den nyeste spilleren for å spille dette spillet",
"Ingen spill funnet, last ned noen først",
"Ingen cache-katalog, bør aldri skje",
"Kan ikke koble til, prøv siste nytt",
"Feil ved nedlasting av innholdselementer",

"Skriv inn spilllisensnøkkelen",
"Kobler til server",
"Laster ned eiendeler",

"Lokalt",
"På nett",
"LAN",

"A"
