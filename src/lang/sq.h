/*
 * tngp/lang/sq.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Albanian dictionary for TirNanoG Player
 *
 */

"sq",

"SDL nuk mund të inicializohet",
"Gabim në ndarjen e memories",
"Nuk është një skedar i vlefshëm TNG",
"Përmirësoje në lojtarin më të fundit për të luajtur këtë lojë",
"Nuk u gjet asnjë lojë, së pari shkarkoni disa",
"Asnjë direktori cache, nuk duhet të ndodhë kurrë",
"Nuk mund të lidhet, provo sërish më së fundi",
"Gabim gjatë shkarkimit të punëve",

"Fut çelësin e licencës së lojës",
"Duke u lidhur me serverin",
"Po shkarkon asetet",

"Lokal",
"Online",
"LAN",

"A"
