TirNanoG Server Translator
==========================

You can configure the server to automatically translate all chat messages using an arbitrary webtranslation service.
The languages are detected from the player's preferences, and if they differ (and the message does not start with a
single `"` quote character prefix), then a translated message arrives to the listener.

You can pass the name of the JSON configuration file to `tngs` executable in the `-t` command line flag. This JSON has
the following fields:

| Field       | Description                                         |
|-------------|-----------------------------------------------------|
| `URL`       | The webservice's URL, optionally with query string  |
| `Method`    | Either "GET" or "POST"                              |
| `Param`     | The HTTP query's parameter                          |
| `Result`    | Returned JSON's field                               |
| `Headers`   | Optional, an array of strings                       |
| `Cookies`   | Optional, an array of strings                       |
| `CookieURL` | Optional, an URL to collect cookies from            |

The `URL` is the webservice's entry point, only "https://" URLs are accepted. You should not specify query string in it
(unless both GET and POST parameters required), there's a `Param` field for that.

In the `Param` field you can use `{from}` (replaced by the sender's language), `{to}` (replaced by the receiver's language)
and `{msg}` (replaced by the URL encoded message). Lanugage codes use the ISO-639-1 two letter codes, like "en", "es", "de",
etc. If you need the three letter codes, use uppercase `{FROM}` or `{TO}` those will use codes like "eng", "spa", "deu", etc.

The webservice's response should be a JSON string, and `Result` specifies which JSON key contains the translated text in that
returned JSON, for example `responseData.translatedText` or `0.0.0` (if the response is an array of array or array and the
string is in the first index).

The `Headers` field is an array of strings, and can be used to add any HTTP Headers to the request that the webservice
might need, like API keys, tokens, etc.

The `Cookies` field is similarily an array of strings, with (key)=(value) pairs, which will be added to the request.

With the `CookieURL`, you can collect the cookies dynamically. If both `Cookies` and `CookieURL` configured, then the
results are concatenated and both are sent to the webservice.

Examples
--------

Apertium for the TirNanoG Server
```json
{
    "URL": "https://apertium.org/apy/translate",
    "Method": "POST",
    "Param": "langpair={FROM}%7C{TO}&q={msg}",
    "Result": "responseData.translatedText"
}
```

Google Translate for the TirNanoG Server
```json
{
    "URL": "https://translate.googleapis.com/translate_a/single",
    "Method": "GET",
    "Param": "client=gtx&sl={from}&tl={to}&dt=t&q={msg}",
    "Result": "0.0.0"
}
```
