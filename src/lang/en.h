/*
 * tngp/lang/en.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief English dictionary for TirNanoG Player
 *
 */

"en",

"Unable to initialize SDL",
"Memory allocation error",
"Not a valid TNG file",
"Upgrade to the latest player to play this game",
"No games found, download some first",
"No cache directory, should never happen",
"Unable to connect, retry later",
"Error downloading assets",

"Enter game license key",
"Connecting to server",
"Downloading assets",

"Local",
"Online",
"LAN",

"A"

