TirNanoG Player Default Paths
=============================

Life would be too simple if we could use the same directory paths on every platform. But we can't so here goes a table:

| OS         | Games                         | Cache                           | Config                    | Saved Games                    |
|------------|-------------------------------|---------------------------------|---------------------------|--------------------------------|
| Windows    | `C:\Program Files\*\game.tng` | `%HOME%\AppData\TirNanoG\cache` | `%HOME%\AppData\TirNanoG` | `%HOME%\Saved Games\TirNanoG`  |
| Linux      | `/usr/share/games/*/game.tng` | `$HOME/.cache/TirNanoG`         | `$HOME/.config/TirNanoG`  | `$HOME/.config/TirNanoG/saves` |
| Android    | `(files)/../../../../Download/*/game.tng` | `(files)/../cache`  | `(files)/`                | `(files)/saves`                |
| Emscripten | *(one file selected by user)* | *(not supported)*               | *(not supported)*         | *(not supported)*              |

Under Android, `(files)` is queried with context.getExternalFilesDir, and most likely `/storage/sdcard0/Android/data/org.tirnanog.app/files`
or `/storage/emulated/0/Android/data/org.tirnanog.app/files`. Using relative path from there makes it irrelevant how the absolute path starts.

Games
-----

This directory holds sub-directories with the games. Each sub-directory must have at least a `game.tng` file, but could have
more, if the game has DLC extensions. For commercial, encrypted game files, you can place the license key in `key.txt` (if that
file cannot be found, then the TirNanoG Player will prompt the user for the decryption key and will store it in `licenses.json`).

Cache
-----

Used to store the game assets downloaded from the server. The player automatically updates this cache, so you can safely delete
this if you run low on free storage space.

Config
------

This directory holds the TirNanoG Player configuration files, common to all games.

| File                   | Description                                                                                           |
|------------------------|-------------------------------------------------------------------------------------------------------|
| `config.json`          | Basic configuration like music volume etc.                                                            |
| `licenses.json`        | For commercial game files, the user provided keys are stored here                                     |
| `gamecontrollerdb.txt` | Optionally you can use your own game controller database. This is a plain text file.                  |

In the `config.json`, you can use the following fields:

| Field            | Description                            |
|------------------|----------------------------------------|
| `lang`           | two letter ISO-639-1 language code     |
| `res_w`, `res_h` | screen's resolution, width and height  |
| `musvol`         | music volume (0 - 255)                 |
| `sfxvol`         | sound effects volume (0 - 255)         |
| `spcvol`         | speech and voice chat volume (0 - 255) |
| `gptres`         | gamepad joystick treshold (1 - 32767)  |

The `lang` only sets the interface's and error messages' language, but the game could be in English if the appropriate
translation isn't found in the game file.

For `gptres`, the default treshold is 8000. The smaller the number is, the more responsive your joysticks will be.

NOTE: an up-to-date version of the [gamecontrollerdb.txt](https://github.com/gabomdq/SDL_GameControllerDB) is embedded in the
player and loaded automatically on startup. You should only create your own db file if you want to re-define the default bindigs,
or if you have an obscure controller not covered by the built-in database (in that case you're probably familiar with this
db file already).

Saved Games
-----------

Sub-directories named `(GameID)` will be created in this directory for each game, and saved games will be stored in that
sub-directory. For more details, see [docs/savegame.md](savegame.md) in the repo.
