/*
 * tngp/lang/et.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Estonian dictionary for TirNanoG Player
 *
 */

"et",

"SDL-i ei saa lähtestada",
"Mälu eraldamise viga",
"Pole kehtiv TNG-fail",
"Selle mängu mängimiseks minge üle uusimale mängijale",
"Mänge ei leitud, laadige esmalt mõned alla",
"Vahemälu kataloogi pole, seda ei tohiks kunagi juhtuda",
"Ühendust ei saa luua, proovige hiljemalt uuesti",
"Viga varade allalaadimisel",

"Sisestage mängu litsentsi võti",
"Ühenduse loomine serveriga",
"Varade allalaadimine",

"Kohalik",
"Internetis",
"LAN",

"A"
