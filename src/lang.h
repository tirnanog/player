/*
 * tngp/lang.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Multilanguage support, dictionary keywords
 *
 */
#ifndef LANG_H
#define LANG_H
enum {
    ERR_DISPLAY,        /*** for practical reasons, line numbers must  ***/
    ERR_MEM,            /*** match with the strings in lang/(code).h files! ***/
    ERR_TNG,
    ERR_COMPAT,
    ERR_NOGAMES,
    ERR_NOCACHE,
    ERR_CONNECT,
    ERR_DOWNLOAD,

    LICENSE_KEY,
    CLIENT_CONNECT,
    CLIENT_DOWNLOAD,

    LAUNCHER_LOCAL,
    LAUNCHER_ONLINE,
    LAUNCHER_LAN,

    DEFAULT_KBD,

    /* must be the last */
    NUMTEXTS
};

#define NUMLANGS         40

extern char **lang;

#endif
