/*
 * tngp/lang/nl.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Dutch dictionary for TirNanoG Player
 *
 */

"nl",

"Kan SDL niet initialiseren",
"Geheugentoewijzingsfout",
"Geen geldig TNG-bestand",
"Upgrade naar de nieuwste speler om deze game te spelen",
"Geen games gevonden, download er eerst een",
"Geen cachemap, zou nooit mogen gebeuren",
"Kan geen verbinding maken, probeer het laatste opnieuw",
"Fout bij downloaden van middelen",

"Voer de licentiesleutel van het spel in",
"Verbinden met de server",
"Middelen downloaden",

"Lokaal",
"Online",
"LAN",

"A"
