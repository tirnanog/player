TirNanoG Player Android Port
============================

I'm looking for a contributor who would help me with this port. I've set up everything as SDL/docs/README.android said, but I
couldn't get ndk-build working, so I couldn't test it and I don't know if this gradle setup is okay or not. Should be to my
best knowledge.

Requirements
------------

Android SDK (version 26 or later)
https://developer.android.com/sdk/index.html

Android NDK r15c or later
https://developer.android.com/tools/sdk/ndk/index.html

Minimum API level supported by SDL: 16 (Android 4.1)

Here's an explanation of the files in the Android project, so you can customize them:

    android-project/app
        build.gradle            - build info including the application version and SDK
        src/main/AndroidManifest.xml    - package manifest. Among others, it contains the class name of the main Activity and the package name of the application.
        jni/                    - directory holding native code
        jni/Application.mk      - Application JNI settings, including target platform and STL library
        jni/Android.mk          - Android makefile that can call recursively the Android.mk files in all subdirectories
        jni/CMakeLists.txt      - Top-level CMake project that adds SDL as a subproject
        jni/SDL/                - (symlink to) directory holding the SDL library files
        jni/SDL/Android.mk      - Android makefile for creating the SDL shared library
        jni/src/                - directory holding your C/C++ source
        jni/src/Android.mk      - Android makefile that you should customize to include your source code and any library references
        jni/src/CMakeLists.txt  - CMake file that you may customize to include your source code and any library references
        src/main/assets/        - directory holding asset files for your application
        src/main/res/           - directory holding resources for your application
        src/main/res/mipmap-*   - directories holding icons for different phone hardware
        src/main/res/values/strings.xml - strings used in your application, including the application name
        src/main/java/org/libsdl/app/SDLActivity.java - the Java class handling the initialization and binding to SDL. Be very careful changing this, as the SDL library relies on this implementation. You should instead subclass this for your application.
        src/main/java/org/tirnanog/app/TirNanoG.java - the main class that extends SDLActivity

Run `./gradlew installDebug` or `./gradlew installRelease` in the project directory. It will build and install your .apk on any connected Android device

Assets
------

Doesn't matter, all assets are embeded in the TirNanoG Player, in the `data.c` source file. If that file is missing, then run

```sh
$ make -C ../src data.c
```

(Sorry, I was unable to figure out how to check for missing files and call make from a gradle build script.)

Accepting Drop Files and URLs
-----------------------------

Not yet implemented.

From what I've gathered, one would have to switch to ANDROID-26 API level, and add an intent to the manifest. Then in
TirNanoG.java, one would have to call Intent.getData() and pass that returned string to the SDLActivity somehow. The
C code in tngp.c expects that string as a normal command line argument (in main()'s argv).

In the meantime, if no game.tng file (nor URL) was given on the command line, then TirNanoG Player fires up a launcher,
which is looking for games in the "Download" directory (see docs/paths.md for more details).
