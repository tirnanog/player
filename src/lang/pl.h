/*
 * tngp/lang/pl.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Polish dictionary for TirNanoG Player
 *
 */

"pl",

"Nie można zainicjować SDL",
"Błąd przydziału pamięci",
"Nieprawidłowy plik TNG",
"Zaktualizuj do najnowszego gracza, aby zagrać w tę grę",
"Nie znaleziono gier, najpierw pobierz niektóre",
"Brak katalogu pamięci podręcznej, nigdy nie powinno się zdarzyć",
"Nie można się połączyć, spróbuj najpóźniej",
"Błąd podczas pobierania zasobów",

"Wprowadź klucz licencyjny gry",
"Łączenie z serwerem",
"Pobieranie zasobów",

"Lokalny",
"Online",
"LAN",

"A"
