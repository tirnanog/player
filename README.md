TirNanoG Role Playing Game Player
=================================

<img src="https://codeberg.org/tirnanog/player/raw/main/src/misc/tngp.png">

[TirNanoG Player](https://tirnanog.codeberg.page/player) is designed to play *Adventure and Action RPG Games*.
For more information, visit the [homepage](https://tirnanog.codeberg.page/player).

Installation
------------

### Step 1: Download

| Platform   | Link                                                                                                           |
|------------|----------------------------------------------------------------------------------------------------------------|
| Windows    | [tngp-i686-win-static.zip](https://codeberg.org/tirnanog/player/raw/binaries/tngp-i686-win-static.zip)         |
| Linux      | [tngp-x86_64-linux-static.tgz](https://codeberg.org/tirnanog/player/raw/binaries/tngp-x86_64-linux-static.tgz) |
| Ubuntu     | [tngp_1.0beta-amd64.deb](https://codeberg.org/tirnanog/player/raw/binaries/tngp_1.0beta-amd64.deb)             |
| RaspiOS    | [tngp_1.0beta-arm64.deb](https://codeberg.org/tirnanog/player/raw/binaries/tngp_1.0beta-arm64.deb)             |
| Android    | [org.tirnanog.app.apk](https://codeberg.org/tirnanog/player/raw/binaries/org.tirnanog.app.apk)                 |
| Emscripten | [tngp-wasm-emscripten.tgz](https://codeberg.org/tirnanog/player/raw/binaries/tngp-wasm-emscripten.tgz)         |

### Step 2: Unpack

Extract to: `/usr` (Linux, BSDs), `C:\Program Files` (Windows); or in a webserver's public folder (Emscripten).

This is a portable executable, no installation required. Alternatively if you've downloaded a deb file then you can install that with

```
sudo dpkg -i tngp_*.deb
```

### Step 3: Add Some Games

You can find demo games on the [homepage](https://tirnanog.codeberg.page/player), but you can also create your own with the
[TirNanoG Editor](https://tirnanog.codeberg.page).

Sadly the diretory where the games should be extracted differs on each platform, see [docs/paths.md](https://codeberg.org/tirnanog/player/src/branch/main/docs/paths.md)
for the default locations. In a nutshell by default it is `/usr/share/games` (on Linux, BSDs etc.), but `C:\Program Files` on
Windows, and the `Download` folder on Android. For Emscripten, you drag'n'drop the game file into the browser window, so location
doesn't matter.

To get some help about how to navigate in a game, see [docs/controls.md](https://codeberg.org/tirnanog/player/src/branch/main/docs/controls.md).

Dependencies
------------

- libc

Yeah, no mistake! This is how a [suckless](https://suckless.org) project looks like! For compilation from source, see the
[readme](https://codeberg.org/tirnanog/player/src/branch/main/src/README.md) in the `src` directory.

Unlike the tarball, the deb version depends on the SDL2 shared library too, but that's where the list of dependencies ends.

Multiplayer Mode
----------------

Normally you add a game file to the *TirNanoG Player*, and play it locally. However you could add it to the *TirNanoG Server*
instead, and then you can use your *TirNanoG Player* to connect to that server to play together with friends.

| Platform   | Link                                                                                                           |
|------------|----------------------------------------------------------------------------------------------------------------|
| Windows    | [tngs-i686-win-static.zip](https://codeberg.org/tirnanog/player/raw/binaries/tngs-i686-win-static.zip)         |
| Linux      | [tngs-x86_64-linux-static.tgz](https://codeberg.org/tirnanog/player/raw/binaries/tngs-x86_64-linux-static.tgz) |
| Ubuntu     | [tngs_1.0beta-amd64.deb](https://codeberg.org/tirnanog/player/raw/binaries/tngs_1.0beta-amd64.deb)             |

If you run the `tngs` application from the application menu, then it will display a very basic launcher window to specify the
bind address (tries to autodetect that) and to select the game file (from the default games directory).

<img src="https://codeberg.org/tirnanog/player/raw/main/docs/tngs.png">


But you really should run it from a terminal (or CMD.EXE) instead, because then you'll have lot more options:
```
tngs [-f] [-p <port>] [-b <ip>] [-a <ip>] [-m <num>] [-l <log>] [-s <save>] [-S <sec>]
  [-t <config>] [-r] [-v|-vv] <game file>

-f            run in foreground, do not daemonize
-p <port>     port to listen on (default 4433)
-b <ip>       bind server to this IP
-a <ip>       allow admin connection from this IP
-m <num>      number of maximum player connections
-l <log>      redirect log to this file
-s <save>     save game state to this file
-S <sec>      autosave game every N seconds (default 3600)
-t <config>   use translator config file (in JSON format)
-r            disable new player registration
-v|-vv        set verbosity level
```

The *TirNanoG Server* is **only supported on Linux**, although should work out-of-the-box on Windows too, but no guarantees.

You can administer the server and manipulate the game world on-the-fly using a simple JSON based [REST API](https://codeberg.org/tirnanog/player/src/branch/main/docs/admin.md),
and you can also configure an automatic [chat translator](https://codeberg.org/tirnanog/player/src/branch/main/docs/translator.md) webservice.

License
-------

The TirNanoG Player is a Free and Open Source software, licensed under *GPLv3* or any later version of that license.

Authors
-------

- [SDL](https://www.libsdl.org) (zlib) Sam Lantinga
- [SDL_mixer](https://www.libsdl.org/projects/SDL_mixer) (zlib) Sam Lantinga
- [libtheora](https://theora.org) (BSD-3clause) Xiph.org Foundation
- [libvorbis](https://xiph.org/vorbis) (BSD-3clause) Xiph.org Foundation
- [mbedtls](https://tls.mbed.org) (Apache-2.0) various contributors
- [NetQ](https://gitlab.com/bztsrc/netq) (MIT) bzt
- [stb_image](https://github.com/nothings/stb) (PD) Sean Barrett and others
- [SSFN2](https://gitlab.com/bztsrc/scalable-font2) (MIT) bzt
- [zlib](https://zlib.net) (zlib) Jean-loup Gailly, Mark Adler
- [on-screen touch controls](https://thoseawesomeguys.com/prompts) (CC0) Nicolae Berbece
- [Celtic md](https://www.fontmeme.com/fonts/celtic-md-font/) font (free, even for commercial use) unknown origin, modified by bzt
- [univga](http://www.inp.nsk.su/~bolkhov/files/fonts/univga/) (MIT) Dmitry Bolkhovityanov
- [unifont](http://unifoundry.com/unifont/index.html) (GPLv2+ with the GNU font embedding exception) Roman Czyborra, Paul Hardy and others
- all the other assets (GPLv3+) bzt
- TirNanoG Player (GPLv3+) bzt
- [TirNanoG File Format](https://codeberg.org/tirnanog/editor/src/branch/main/LICENSE) (dual licensed CC-by-nc-sa or anything else, including proprietary) bzt

NOTE: by the terms of [GPL](LICENSE) section 5c, all these libraries and assets embedded in the TirNanoG Player are re-licensed
under GPLv3+ (or any later version of that license).

Known Bugs
----------

The DLC expansion system (where you load further .tng files to patch or replace contents in the main .tng file) is implemented
and should work, but hasn't been tested throughfully yet. Best to stick to one game .tng file for now.

Likewise, big endianness is implemented, but hasn't been throughfully tested yet, because I don't have any big endian machines.
The networking code (little endian client connecting to big endian server or vice versa) and game save files will work for sure.
I've implemented both with the outtermost care. Loading .tng should work too, all the necessary macro calls are in place, but
there could be bugs with the latter on big endian machines. Definitely needs more testing.

Cheers,
bzt
