TirNanoG Player Controls
========================

These are always the same, no matter the game.

Keyboard
--------

![keyboard](keyboard.png)

1. <kbd>Esc</kbd> game menu or exit HUD windows
2. <kbd>Tab</kbd> inventory window (by pressing multiple times, it cycles through inventory tabs and inputs)
3. <kbd>1</kbd>...<kbd>9</kbd> quick access items on the belt
4. <kbd>Ctrl</kbd> default action (in menus, <kbd>Enter</kbd>)
5. <kbd>arrows</kbd> player movement
6. any key, start text chat (multiplayer only)
7. <kbd>Super</kbd> keep pressed for voice chat (multiplayer only)

Mouse and Touchscreens
----------------------

Depends on where you click (or tap) on the screen.

![mouse](mouse.png)

1. game menu
2. inventory window (click or tap on the tabs to change them)
3. quick access items on the belt
4. default action
5. player movement

With mouse, you probably want to click on the center of the screen for movement and action. However with tapping it can be
annoying that you can't see your character from your own hand, so the touchscreen controls can be found on the bottom left and
right corners as well (on desktop the touch-screen controls can be enabled with the `-t` command line flag). Chat is accessible
from the game menu.

Normally with touch-screen devices when text has to be entered, then the OS provided on-screen keyboard is shown. But you can
turn that off and use the built-in on-screen keyboard (see below) instead with the `-k` command line flag.

Game Controller
---------------

![controller](controller.png)

1. <kbd>Left shoulder</kbd> game menu or exit HUD windows
2. <kbd>Right shoulder</kbd> inventory window (by pressing multiple times, it cycles through inventory tabs)
3. <kbd>A</kbd>, <kbd>X</kbd>, <kbd>Y</kbd> quick access items on the belt
4. <kbd>B</kbd> default action
5. <kbd>Left joystick</kbd>, <kbd>Right joystick</kbd> player movement (however they might control different HUD windows)
6. <kbd>Start</kbd> start text chat (multiplayer only)
7. <kbd>Guide</kbd> keep pressed for voice chat (multiplayer only)

If you want to reassign buttons, you can provide your [gamecontrollerdb.txt](paths.md) in the configuration directory. You
can also set the responsiveness of the joystick in the `gptres` field of the `config.json` file located in the same directory.

![onscreenkbd](onscreenkbd.png)

When a gamepad controller is detected and text has to be entered, then an on-screen keyboard automatically pops up, so that you
can type with a controller too. This is a very limited and simple on-screen keyboard without BiDi input method, but supports
numbers, the most common symbols and currency signs, all uppercase and lowercase Latin, Greek, Cyrillic, Hebrew, Hirakana,
Katakana and Devanagari letters. It isn't perfect, isn't particularly comfortable to use, but at least more than nothing.
